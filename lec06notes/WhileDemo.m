clear

x = input('Enter a number 0 or greater: ');

while x<0  |  numel(x)~=1
    if any(x<0)
        fprintf('Non-negative only!\n');
    end
    if numel(x)~=1
        fprintf('One element only\n');
    end
    x = input('Enter a number 0 or greater: ');
end