clear

% Price = rand(1);
Price = randi([100 1000]);

Guess = input('Guess: ');

tic;
while Price ~= Guess  & toc<30
    fprintf('Wrong!\n')
    if Guess < Price
        fprintf('Higher!\n')
    else
        fprintf('Lower!\n')
    end
    Guess = input('Next Guess: ');
    LastGuessTime = toc;
end


if Price==Guess &  LastGuessTime<=30
    fprintf('You win!\n')
else
    fprintf('LOSER\n')
end