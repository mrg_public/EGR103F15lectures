% One shot of RK4
function OneShotRK4

% RK4 code from rk4demo.m, from
%%%  http://www.mece.ualberta.ca/Courses/me390/390code/examples.htm
% File provided by site above with following note:
%%% You can use these as templates for your own programs, but remember that these are  
%%% supplied without warranty. There may be bugs in these programs - buyer beware!
%%% Also, you should note that these programs are written for the purpose of
%%% demonstration, and may not be written as efficiently as possible, where extra
%%% efficiency might cloud the issue. For example, for the most part these programs    
%%% make very little use of MATLAB's vector capabilities.

  x  = 0; % start indep value
  h  = 1; % step size
yRK1 = 2; % starting dependent value
  QP = 1; % flag to add quiver plot

k1 = mydiff(x,         yRK1);                % 4th order Runge-Kutta solution
k2 = mydiff(x + h / 2, yRK1 + h * k1 / 2);
k3 = mydiff(x + h / 2, yRK1 + h * k2 / 2);
k4 = mydiff(x + h,     yRK1 + h * k3);

yt1 = yRK1 + h * k1 / 2
yt2 = yRK1 + h * k2 / 2
yt3 = yRK1 + h * k3

% ignore: just for plotting
x1 = x; xh = x1+h/2; x2 = x+h;
ss = 6;
xm1 = xh-h/ss; xm2 = xh+h/ss; sh = h/ss; 

% calculate new value
yRK2 = yRK1 + (k1 + 2*k2 + 2*k3 + k4)*h / 6

% plot stuff
figure(1)
plot(x1, yRK1, 'ko')
hold on
plot([x1-h/ss x1+h/ss], [yRK1-sh*k1 yRK1+sh*k1], 'r-', 'LineWidth', 3)
plot([x xh], [yRK1,  yt1], 'r--', xh, yt1, 'ro',  'LineWidth', 3)
plot([xm1 xm2], [yt1-sh*k2 yt1+sh*k2], 'm-', 'LineWidth', 3)
plot([x xh], [yRK1,  yt2], 'm--', xh, yt2, 'mo', 'LineWidth', 3)
plot([xm1 xm2], [yt2-sh*k3 yt2+sh*k3], 'b-', 'LineWidth', 3)
plot([x x2], [yRK1,  yt3], 'b--', x2, yt3, 'bo','LineWidth', 3)
plot([x2-h/ss x2+h/ss], [yt3-sh*k4 yt3+sh*k4], 'g-', 'LineWidth', 3)
plot([x x2], [yRK1, yRK2], 'k--', x2, yRK2, 'ko', 'LineWidth', 3)
hold off
legend(...
'Start', ...
'Slope k1', 'Shot of k1', 'End of k1', ...
'Slope k2', 'Shot of k2', 'End of k2', ...
'Slope k3', 'Shot of k3', 'End of k3', ...
'Slope k4', 'Shot of \phi', 'End')
gzoom
xe = get(gca, 'XLim'); ye = get(gca, 'YLim');

if QP
[x, y] = meshgrid(linspace(xe(1), xe(2), 30), linspace(ye(1), ye(2), 30));
px = x.^0+0.1*diff(xe);
py = mydiff(x, y).*px;
hold on
quiver(x,y,px,py, 'k')
hold off
axis([xe ye])
end
end

function dydx = mydiff(x, y)
%dydx = 1;
%dydx = x;
%dydx = y;
dydx = cos(x);
%dydx = sin(y);
dydx = cos(x)+sin(y);
end