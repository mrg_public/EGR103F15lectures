function LakeDrain

h = 6:-1:0;
A = [1.17 0.97 0.67 0.45 0.32 0.18 0.00]*1e4;
hmodel = linspace(min(h), max(h), 100);
Amodel = spline(h, A, hmodel);
%% Profile assuming constant width lake
figure(1); clf
plot(1.2e4-A, h, 'r+',...
     1.2e4-Amodel, hmodel, 'r:', ...
     1.2e4+A*0, h, 'k-')

%% Solve ODE 
[tout, yout] = ode45(@(t, y) LakeODE(t, y, h, A), linspace(0, 70e3, 1000), 6);

%% Height graph
figure(2); clf
plot(tout, yout);
xlabel('Time (s)'); ylabel('Height (m)'); title('Height vs. Time (mrg)');

%% Draining animation
figure(3); clf
axis([0 1.4e4 0 6])
MyLake = fill(1.2-[Amodel, 0, 0], [hmodel, 6, 0], 'b');
hold on
plot(1.2e4-Amodel, hmodel, 'k-', ...
     1.2e4+A*0, h, 'k-')
for k=1:length(tout)
    TopH = yout(k);
    newhmodel = linspace(0, TopH, ceil(100*TopH/6));
    newAmodel = spline(h, A, newhmodel);
    set(MyLake, 'XData', 1.2e4-[newAmodel, 0, 0], ... 
        'YData', [newhmodel, TopH, 0]);
    axis([0 1.4e4 0 6])
    
    drawnow
end

end

function dydt = LakeODE(t, y, hdata, Adata)
d = 0.25;
e = 1.00;
g = 9.81;
if y<0 dydt=0; return; end % stop draining once drained
dydt = -pi*d.^2 * sqrt(2*g*(y+e)) ./ 4 ./ spline(hdata, Adata, y);
end