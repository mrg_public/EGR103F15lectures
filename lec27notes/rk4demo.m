%%  File from: 
%%% http://www.mece.ualberta.ca/Courses/me390/390code/examples.htm
%   File provided by site above with following note:
%%% You can use these as templates for your own programs, but remember that these are  
%%% supplied without warranty. There may be bugs in these programs - buyer beware!
%%% Also, you should note that these programs are written for the purpose of
%%% demonstration, and may not be written as efficiently as possible, where extra
%%% efficiency might cloud the issue. For example, for the most part these programs    
%%% make very little use of MATLAB's vector capabilities.

%%  File modified by Michael Gustafson to fit
%%% Example 20.3, pp. 496-498 in 
%%% "Applied Numerical Methods in Matlab," 2nd ed, by S. Chapra
%%% Major modifications to code order and storage structures
%%% Euler and RK4 methods left intact
%%% ode45 code updated to anonymous function handles
%%% 11/2014 updates to indep. array; error checking

%% ------

% 4th order Runge-Kutta Demo
%
%
%      file fcn1.m        function dyxy = fcn1(x,y)
%                         dydx = y - sin(x) + cos(x);
%
%  exact solution is, y(x) = SIN(x) + EXP(x) * y(0)

% initial setup
xmin   =   0;    
xmax   =   5;
y0     =   1;
yRK    =  y0;
yeuler = yRK;

h        = input('Enter step size (<= 2.5):       ');
while length(xmin:h:xmax)<=2
    h        = input('TOO LARGE - Enter smaller step size: ');
end
printint = input('Enter print interval (integer): ');
while mod(printint,1)~=0
    printint = input('NOT AN INTEGER - Enter print interval: ');
end
    
fprintf('\n\n  Step size: %g   print interval: %g\n\n', h, printint);

% One step Matlab solution...
x = xmin:h:xmax;
[xODE45, yODE45] = ode45(@(x,y) fcn1(x,y), x, y0);

for k=1:(length(x)-1)
  yeuler(k+1) = yeuler(k) + fcn1(x(k), yeuler(k)) * h;    % Euler solution
 
  k1 = fcn1(x(k),         yRK(k));                % 4th order Runge-Kutta solution
  k2 = fcn1(x(k) + h / 2, yRK(k) + h * k1 / 2);
  k3 = fcn1(x(k) + h / 2, yRK(k) + h * k2 / 2);
  k4 = fcn1(x(k) + h,     yRK(k) + h * k3);

  yRK(k+1) = yRK(k) + (k1 + 2*k2 + 2*k3 + k4)*h / 6;
end;


yEXACT = sin(xODE45) + exp(xODE45) * y0;
fprintf('\n           x     y Euler      y RK4      ode45      exact\n');
fprintf('IC: %8.4f  %10.4f %10.4f %10.4f %10.4f\n',x(1),...
    yeuler(1), yRK(1), yODE45(1), yEXACT(1));
for i = 1+printint:printint:length(xODE45)
  fprintf('%12.4f  %10.4f %10.4f %10.4f %10.4f\n',x(i),...
      yeuler(i), yRK(i), yODE45(i), yEXACT(i));
end;

plot(x, yeuler, 'k-', ...
     x, yRK,    'r--', ...
     x, yODE45, 'b-.', ...
     x, yEXACT, 'g:')
 legend('Euler', 'RK4', 'ODE45', 'Exact', 0)
