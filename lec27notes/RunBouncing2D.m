% Run Bouncing2D

% Set up time base - two options
% 1) start and finish 
%    tspan = [ts tf]
% 2) array of times to solve
%    tspan = linspace(ts, tf, N)
%    OR tspan = ts:dt:tf
tspan = 0:.1:40;

% Set up initial values
% yinit = [ry0; vy0; rx0; vx0]
yinit = [0, 20, 0, 10]';

% Set up options
% Use derivative function itself to handle events
options = odeset('Events',... 
    @(t,y)BouncingDiff2D(t,y,'events'));

% Run ode of choice 
% (ode23, ode23s, ode45, etc)
tout = [];
yout = [];
while min(tspan)<20
    [tseg, yseg, te, ye, ie] = ode45(@(t,y)BouncingDiff2D(t,y),...
        tspan, yinit, options);
    tspan = te(end):.1:max(tspan);
    yinit = [0 -.89*yseg(end,2) yseg(end,3) yseg(end,4)];
    tout = [tout; tseg];
    yout = [yout; yseg];
end

% Pull out position and velocity information
ry = yout(:,1);
vy = yout(:,2);
rx = yout(:,3);
vx = yout(:,4);
t = tout;

% Plot...if you want to
figure(1)
plot(rx, ry, 'b-o')
xlabel('x');
ylabel('y');

figure(2)
subplot(2,2,1)
plot(t, rx, 'b-o')
xlabel('t');
ylabel('x');
subplot(2,2,2)
plot(t, ry, 'b-o')
xlabel('t');
ylabel('y');
subplot(2,2,3)
plot(t, vx, 'b-o')
xlabel('t');
ylabel('v_x');
subplot(2,2,4)
plot(t, vy, 'b-o')
xlabel('t');
ylabel('v_y');