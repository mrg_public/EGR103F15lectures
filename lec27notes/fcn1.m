function dydx = fcn1(x, y)
dydx = y - sin(x) + cos(x);
