% Run Bouncing2DPlane
figure(1); clf
% Set up time base - two options
% 1) start and finish 
%    tspan = [ts tf]
% 2) array of times to solve
%    tspan = linspace(ts, tf, N)
%    OR tspan = ts:dt:tf
tspan = 0:.1:60;

% Set up initial values
% yinit = [ry0; vy0; rx0; vx0]
yinit = [0, 20, 0, 20]';

% Coefficient of restitution
COR = 0.05;
 
% Set up options
% Use derivative function itself to handle events
options = odeset('Events',... 
    @(t,y)BouncingDiff2DPlane(t,y,'events'));

% Run ode of choice 
% (ode23, ode23s, ode45, etc)
tout = [];
yout = [];
yseg = 100;
while min(tspan)<50 & mean(yseg(:,1))>.01 % time left and moving fast enough
    [tseg, yseg, te, ye, ie] = ode45(@(t,y)BouncingDiff2DPlane(t,y),...
        tspan, yinit, options);
    tspan = te(end):.1:(te(end)+30); % reset if need be
    if ie==1
        yinit = [yseg(end,1) -COR*yseg(end,2) yseg(end,3) +COR*yseg(end,4)];
    elseif ie==2 | ie==3
        %fprintf('Bounce\n')
        yinit = [yseg(end,1) +COR*yseg(end,2) yseg(end,3) -COR*yseg(end,4)];
    end
    tout = [tout; tseg];
    yout = [yout; yseg];
end

% Pull out position and velocity information
ry = yout(:,1);
vy = yout(:,2);
rx = yout(:,3);
vx = yout(:,4);
t = tout;

% Make Plots
figure(1); clf
plot(tout, yout);
xlabel('Time');
ylabel('State');
legend('y', 'vy', 'x', 'vx', 0)

figure(2); clf
N  = length(tout);
TL = 28;
k  = 1;
xp = rx(max(1,k-TL):min(N, k));
yp = ry(max(1,k-TL):min(N, k));
TailPlot=plot(xp, yp, 'k-');
axis equal
hold on
BallPlot = plot(xp(end), yp(end), 'ko',...
    'MarkerSize', 5, 'MarkerFaceColor', 'k');
patch([0   0 -1 -1], [0 30 30 -1], 'r')
patch([0  -1 21 20], [0 -1 -1  0], 'g')
patch([20 20 21 21], [0 30 30 -1], 'b')
hold off
drawnow

for k=1:N+TL
    xp = rx(max(1,k-TL):min(N, k));
    yp = ry(max(1,k-TL):min(N, k));
    set(TailPlot, 'XData', xp, 'YData', yp);
    set(BallPlot, 'XData', xp(end), 'YData', yp(end));
    drawnow
    pause(0.01)
end


    