function [yprime, isterminal, dir] = BouncingDiff2D(t, y, flag)
% This uses 
%   y(1) as y position
%   y(2) as y velocity
%   y(3) as x position
%   y(4) as x velocity
% such that
%   yprime(1) is y velocity
%   yprime(2) is y acceleration
%   yprime(3) is x velocity
%   yprime(4) is x acceleration
% 
% flag's existence means a value crossed 0

% Set up non-flagged entries
% if statement from Palm, p. 529
if nargin<3 | isempty(flag)
    % The derivative of y position is velocity
    yprime(1) = y(2);

    % The derivative of y velocity is acceleration,
    % in this case, due to gravity
    g = -9.81;
    yprime(2) = g;
    
    % The derivative of x position is x velocity
    yprime(3) = y(4);

    % The derivative of x velocity is x acceleration,
    % in this case, 0
    yprime(4) = 0;
    
    % Must send derivative as a column
    yprime = yprime';

elseif strcmp(flag, 'events')
    % If an event has happened, send current
    % position and velocity back to program
    yprime = y;
    % Terminate current run if the ball bounced,
    % that is, if y(1) crossed through 0
    isterminal = [1; 0; 0; 0];
    % Only care if the y(1) value goes from + to -
    % avoiding the potential sign change at startup
    dir = [-1; 0; 0; 0];
else
    error(['flag unknown'])
end