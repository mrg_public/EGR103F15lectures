function dydt = ExampleDiff(t, y, C)
% Template for calculating first derivatives of state variables
% t is time
% y is the state vector
% C contains any required constants
% dydt must be a column vector
%dydt = C(1);
%dydt = C(1)*t
%dydt = C(1)*y;
%dydt = t+y;
dydt = C(1)*y*(C(2)-y);
%dydt = C(1)*y*(C(2)-y)-7*y.*cos(y).^2;
