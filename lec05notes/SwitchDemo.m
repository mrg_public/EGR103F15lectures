%% Initialize
clear

%% Get a word
MyWord = input('Enter favorite color: ', 's');

%% Report it, then report it backwards
fprintf('The mirror of %s is %s.\n', MyWord, MyWord(end:-1:1));

%% Talk about the color if possible
switch lower(MyWord)
    case {'blue', 'duke blue', 'dark blue'}
        fprintf('Go Blue Devils!\n')
    case 'purple'
        fprintf('Poetry must by hard for you...\n')
    case 'blue'
        error('This will NEVER happen...\n')
    otherwise
        fprintf('I have nothing witty to say about %s...\n', MyWord);
        fprintf('Here''s what it looks like vertically:\n');
        fprintf('    %c\n', MyWord) 
end