%% Initialize
clear

%% Create matrices
A = [1 3 5; 2 4 6]
Z = [3 4 5; 1 1 9]

%% Basic Operations
geq = A>=Z
gt  = A>Z
eq  = A==Z
neq = A~=Z
lt  = A<Z
leq = A<=Z

%% Check boundaries (says all true!)
%  Wrong way!
wrong = 3<A<5
%  Correct way (should find the 4 in element 4)
right = (3<A) & (A<5)

