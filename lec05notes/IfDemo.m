%% Initialize
clear

%% Get a matrix of values
MyNum = input('Enter matrix of values: ');

%% Report the number of entries and stats about the matrix
fprintf('There are %0.0f entries and the matrix is ',...
    numel(MyNum));
fprintf('%0.0f x %0.0f.\n', size(MyNum));
fprintf('The values range from %+0.4e to %+0.4e.\n', ...
    min(MyNum(:)), max(MyNum(:)));
fprintf('The average is %+0.4e.\n', mean(MyNum(:)))


%% Report if all the entries are non-negative
if MyNum >= 0
    fprintf('The matrix is all non-negative.\n');
else
    fprintf('There is at least one negative entry.\n');
end

%% Report if any entries are even and how many
if any(mod(MyNum,2)==0)
    EvenCount = length(find(mod(MyNum,2)==0));
    if EvenCount==1
        fprintf('There is 1 even number.\n');
    else
        fprintf('There are %0.0f even numbers.\n', EvenCount);
    end
else
    fprintf('No even numbers to be found.\n')
end

%% Report if all integers, or no integers, or a mix
%  If a mix, report how many integers and non-integers
if round(MyNum)==MyNum
    fprintf('All integers.\n');
elseif round(MyNum)~=MyNum
    fprintf('No integers.\n')
else
    fprintf('A mix of %0.0f integers and %0.0f non-integers.\n',...
        length(find(round(MyNum)==MyNum)),...
        length(find(round(MyNum)~=MyNum)));
    fprintf('The integers, in order of appearance, are:\n');
    fprintf('%0.0f ', MyNum(find(round(MyNum)==MyNum)));
    fprintf('\nThe integers, sorted from -inf to inf, are:\n');
    fprintf('%0.0f ', sort(MyNum(find(round(MyNum)==MyNum))));
    fprintf('\n');
end