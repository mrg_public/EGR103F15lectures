function [Vol, SA] = Tank(W, L, H)
% Tank  Calculate the volume and surface area of a fish tank
%    [Vol, SA] = Tank(W) calculates the volume and surface 
%    area of an open-top cubic fish tank with width, length, 
%    and height W
% 
%    [Vol, SA] = Tank(W, L, H) calculates the volume and 
%    surface area of an open-top fish tank with width W, 
%    length L, and height H

% Not helpful

if nargin==0 | nargin==2
    error('Won''t work!');
elseif nargin==1
    L=W; H=W;
end

Vol = W*L*H;