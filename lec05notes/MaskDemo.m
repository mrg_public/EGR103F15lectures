%% Initialize
clear

%% Create x and y
x = linspace(-4, 4, 1000);
y = ...
    (x<0)        .* (-1) + ...
    (0<=x & x<2) .* (x-1) + ...
    (x>=2)       .* (1);
plot(x, y, 'k-');
grid on
axis equal
gzoom