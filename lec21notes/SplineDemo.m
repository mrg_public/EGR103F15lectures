clear;

x = (1:15);
x = x';
y = randn(size(x));
xmodel = linspace(min(x), max(x), 1000);

ymodel_P       = polyval(polyfit(x, y, length(x)-1), xmodel);
ymodel_L       = interp1(x, y, xmodel, 'linear');
% ymodel_CS      = interp1(x, y, xmodel, 'spline');
ymodel_CS    = spline(x, y, xmodel)
% ymodel_PCHIP   = interp1(x, y, xmodel, 'pchip');
ymodel_PCHIP = pchip(x, y, xmodel)
% Clamped
ymodel_CSC     = spline(x, [100; y(:); 0], xmodel);
figure(1); clf
plot(x,      y,        'ko', ...  
     ...%xmodel, ymodel_P, 'r:', ...
     xmodel, ymodel_L, 'b:', ...
     xmodel, ymodel_CS, 'g-', ...
     xmodel, ymodel_PCHIP, 'r-', ...
     xmodel, ymodel_CSC, 'm--');
 legend('Data', 'Linear', 'Spline', 'PCHIP', 'Clamped', '0')
