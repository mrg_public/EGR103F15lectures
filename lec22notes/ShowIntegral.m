clear

F    = @(x) cos(x)
IntF = @(x) sin(x)
%F    = @(x) x.^8
%IntF = @(x) x.^9/9
%F    = @(x) exp(-x);
%IntF = @(x) 1-exp(-x);

x = linspace(0, 4*pi, 1000);
for k=1:length(x)
    NewIntF(k) = integral(@(xd) F(xd), 0, x(k));
end

CT = cumtrapz(x, F(x));

plot(x, IntF(x), 'ko', x, NewIntF, 'r:*', x, CT, 'g+--')
legend('Actual Integral', 'integral Command', 'Cumulative Trapezoid', 0)