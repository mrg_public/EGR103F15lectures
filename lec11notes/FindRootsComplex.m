clear; format short e; figure(1); clf

xr = linspace(-4, 4, 200);
xi = linspace(-4, 4, 202);
%find(x0==-1)

r = zeros(length(xi), length(xr));
n = zeros(length(xi), length(xr));

for K=1:length(xr)
    for M=1:length(xi)
        [r(M, K), xev] = newtonGus('Basic', xr(K)+i*xi(M));
        n(M, K) = length(xev);
    end
end

figure(1); clf
plot(xr, Basic(xr), 'k-');
% Create color info
%c = (abs(r-1)<1e-2).*0 + (abs(r)<1e-2).*5 + (abs(r+1)<1e-2).*10;
c = angle(r*exp(-i*pi/180)); % tilt 1 degree to unwrap roundoff
figure(2); clf
subplot(2, 1, 1)
imagesc(xr, xi, c)
colorbar
subplot(2, 1, 2)
imagesc(xr, xi, log10(n))
colorbar
