function [f, df] = Basic(x)
Demo = 1;
switch Demo
    case 1
        f  = x-1;
        df = 1;
    case 2
        f  = x.^2 + 2.*x - 3;
        df = 2*x + 2;
    case 3
        f  = x.^3 - x;
        df = 3*x.^2 - 1;
    case 4
        f  = x.^4 - 1;
        df = 4.*x.^3;
    case 5
        f = 10*sin(x)+x.^2;
        df = 10*cos(x)+2*x;
end
