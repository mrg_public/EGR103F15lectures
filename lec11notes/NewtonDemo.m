%% Initialize workspace
clear; format short e; clc; figure(1); clf

%% Set stopping conditions
ftol    = 1e-1;
MaxIter = 10;

%% Choose demo
MyDemo = 3;
switch MyDemo
    case 1
        f  = @(x) x-1; 
        df = @(x) 1; 
        x(1) = 2.2; xrange = [-4 4];
    case 2
        f  = @(x) x.^2 + 2.*x - 3;
        df = @(x) 2*x + 2;
        x(1) = -1.5; xrange = [-4 4];
    case 3
        f = @(x) x.^3 - x;
        df = @(x) 3*x.^2 - 1;
        x(1) = 0.55; xrange = [-4 4];
    case 4
        f = @(x) x.^4 - 1;
        df = @(x) 4*x.^3;
        x(1) = 0.55; xrange = [-4 4];
    case 5
        f = @(x) 10*sin(x)+x.^2;
        df = @(x) 10*cos(x)+2*x;
        %x(1) = 1.7; xrange = [-9 6];
        x(1) = 1.8; xrange = [-9 6];
    case 6
        f = @(x) exp(-x)-0.5;
        df = @(x) -exp(-x);
        x(1) = -4; xrange = [-4 4];
    case 7
        f = @(x) sin(2*pi*x);
        df = @(x) 2*pi*cos(2*pi*x);
        n=1.5;
        x(1) = atan(-2*pi*n)/2/pi; xrange = [-3, 3];
end

%% Plot equation and first point
xp = linspace(xrange(1), xrange(2), 1000);
plot(xp, f(xp), 'k-')
hold on
plot(x(end), f(x(end)), 'ro')
grid on

%% Start iterator and report values
k = 1;
fprintf('Step %3.0f: x=%+12.3e    f(x)=%+12.3e\n', k, x(end), f(x(end)))

%% Perform Newton Method 
while abs(f(x(end)))>ftol & k<MaxIter
    pause
    k=k+1;
    x(k) = x(k-1) - f(x(k-1))/df(x(k-1));
    % Report results
    fprintf('Step %3.0f: x=%+12.3e    f(x)=%+12.3e\n', k, x(end), f(x(end)))
    plot(x((end-1):end), [f(x(end-1)), 0], 'b--')
    plot(x(end), 0, 'bs')
    plot([x(end) x(end)], [f(x(end)), 0], 'r--')
    plot(x(end), f(x(end)), 'ro')
end

%% Report final results
if abs(f(x(end)))<=ftol
    fprintf('\nRoot at %0.3e\nf(%0.3e)=%0.3e\n', x(end), x(end), f(x(end)))
else
    fprintf('Maximum number of iterations reached\n')
end