clear; format short e; figure(1); clf

x0 = linspace(-8, 8, 1000);
%find(x0==-1)

for K=1:length(x0)
   [r(K), xev] = newtonGus('Basic', x0(K));
   n(K) = length(xev);
end

figure(1); clf
plot(x0, Basic(x0), 'k-');
grid on

figure(2); clf
subplot(2, 1, 1)
imagesc(x0, x0.^0, r)
colorbar
subplot(2, 1, 2)
imagesc(x0, x0.^0, n)
colorbar