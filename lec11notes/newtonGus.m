function [r, xs] = newtonGus(fun, x0, xtol, ftol, verbose, maxit)
if nargin < 2, error('Not enough inputs'); end;
if nargin < 3, xtol = 5*eps; end;
if nargin < 4, ftol = 5*eps; end;
if nargin < 5, verbose = 0; end;
if nargin < 6, maxit = 1000; end;

if verbose
    fprintf('\nNewton iterations for %s.m\n', fun)
    fprintf('  k         x(k)         f(x)         dfdx           dx       x(k+1)\n')
end

x = x0; k = 0;
while k <= maxit;
    k = k + 1;
    xs(k, 1) = x;
    [f, dfdx] = feval(fun, x);
    dx = -f/dfdx;
    x = x + dx;
    if verbose
        fprintf('%3d %12.4e %12.4e %12.4e %12.4e %12.4e\n',...
            k, x-dx, f, dfdx, dx, x);
    end
    if (abs(f) < ftol) | (abs(dx) < xtol)
        r = x;
        xs(end+1) = r;
        return
    end
end
r = x;