clear; format short e

figure(1); clf

f = @(x) x.^3 - x.^2 - 1;

%x = linspace(0, 10, 100);
x = linspace(1.4, 1.5, 100);

plot(x, f(x), 'k-o')
pause
[MyX, MyY] = ginput(1)