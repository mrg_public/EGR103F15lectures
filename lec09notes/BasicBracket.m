clear; format short e;
figure(1); clf

%% Define the function
f = @(x) (x-3).*(x-4).*(x-5)+pi/16;

%% Define the limits and resolution
xmin = 0;
xmax = 6;
nx   = 1234;
x    = linspace(xmin, xmax, nx);

%% Find and report zeros
Roots = x(find(f(x)==0));
NumRoots = numel(Roots);

for k=1:NumRoots
    fprintf('Root number %2.0f: %5e\n', k, Roots(k))
end

%% Find and report brackets
NumBrackets = 0;
BrackInterval = [];
Bracket = [];

for k=1:(nx-1) % which segment
   if sign(f(x(k))).*sign(f(x(k+1)))==-1
    NumBrackets = NumBrackets + 1;
    BrackInterval(NumBrackets) = k;
    Bracket(NumBrackets, :) = [ x(k)  x(k+1)]
   end
    
end


%% Make plots
subplot(2, 1, 1)
plot(x, f(x), 'k-')
pos = find(f(x)>0);
neg = find(f(x)<0);
nul = find(f(x)==0);
hold on
plot(x(pos), f(x(pos)), 'b.')
plot(x(neg), f(x(neg)), 'r.')
plot(x(nul), f(x(nul)), 'go', 'MarkerSize', 18)
hold off

subplot(2, 1, 2)
plot(x, sign(f(x)), 'k-')
axis([xmin xmax -1.1 1.1])
