clear; format long e
Start = 10;
Delta = 0.31251;
Finish = 1000;

K = 1;
MyValC(K) = Start;
MyValA(K) = Start;

while MyValC(K)+Delta <= Finish
    K = K + 1;
    MyValC(K) = MyValC(K-1) + Delta;
    MyValA(K) = Start + (K-1)*Delta;
end;

figure(1); clf
plot(MyValC - MyValA, 'k.')
figure(2); clf
semilogy(abs(MyValC - MyValA), 'k.')