clear; format long e

MyExp = @(x, n) (1 + (x./n)).^n;

MyExp(1, 1)
MyExp(1, 100)
MyExp(1, 10000)

n=logspace(0, 18, 1000);
figure(1); clf
semilogx(n, MyExp(10,n), 'b-')