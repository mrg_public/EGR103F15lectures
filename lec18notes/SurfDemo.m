clear; format short e

%% Load and manipulate data
load SurfaceData

%% Rename and create model data
x1m = xm;
x2m = ym;
ym  = zm;
x1v = x1m(:);
x2v = x2m(:);
yv  = ym(:);
[x1model, x2model] = meshgrid(...
    linspace(min(x1v), max(x1v), 30),...
    linspace(min(x2v), max(x2v), 31));

%% Define model and the A matrix
model = 'best'
switch model
    case 'constant'
        yeqn = @(coefs, x1e, x2e) coefs(1)*x1e.^0;
        A    =                            [x1v.^0];
    case 'plane'
        yeqn = @(coefs, x1e, x2e) coefs(1)*x1e.^0 + coefs(2)*x1e.^1 + coefs(3)*x2e.^1;
        A    =                            [x1v.^0            x1v.^1            x2v.^1];
    case 'not much better'
        yeqn = @(coefs, x1e, x2e) coefs(1)*x1e.^0 + coefs(2)*x1e.^1 + coefs(3)*x2e.^1 + coefs(4)*x1e.^2;
        A    =                            [x1v.^0            x1v.^1            x2v.^1            x1v.^2];
    case 'better'
        yeqn = @(coefs, x1e, x2e) coefs(1)*x1e.^0 + coefs(2)*x1e.^1 + coefs(3)*x2e.^1 + coefs(4)*cos(2*pi*x1e) + coefs(5)*x2e.^2;
        A    =                            [x1v.^0            x1v.^1            x2v.^1            cos(2*pi*x1v)            x2v.^2];
    case 'best'
        yeqn = @(coefs, x1e, x2e) coefs(1)*x1e.^0 + coefs(2)*x1e.^1 + coefs(3)*x2e.^1 + coefs(4)*cos(2*pi*x1e) + coefs(5)*x2e.^2 + coefs(6)*x1e.*x2e;
        A    =                            [x1v.^0            x1v.^1            x2v.^1            cos(2*pi*x1v)            x2v.^2            x1v.*x2v];
end

%% Determine best fit coefficients
MyCoefs = A\yv

%% Generate estimates and models
yhat =   yeqn(MyCoefs, x1v,     x2v);
ymodel = yeqn(MyCoefs, x1model, x2model);

%% Calculate statistics
ybar = mean(yv);
St = sum((yv-ybar).^2)
Sr = sum((yv-yhat).^2)
r2 = (St - Sr) / St

%% Plots
figure(1); clf
meshc(x1m, x2m, ym); colormap copper

figure(2); clf
meshc(x1model, x2model, ymodel); colormap copper

figure(3); clf
meshc(x1m, x2m, ym - yeqn(MyCoefs, x1m, x2m)); colormap copper

