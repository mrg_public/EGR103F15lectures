clear; format short e

%% Load and manipulate data
load Cantilever.dat
Force = Cantilever(:,1) * 9.81;
Disp = Cantilever(:,2) * 2.54 / 100;

%% Rename and create model data
x = Force;
y = Disp;
xmodel = linspace(min(x), max(x), 100);

%% Define model and the A matrix
model = 'linear'
switch model
    case 'linear'
        yeqn = @(coefs, x) coefs(1)*x.^1 + coefs(2)*x.^0
        InitGuess = [1 0];
    case 'power'
        yeqn = @(coefs, x) coefs(1)*x.^(coefs(2))
        InitGuess = [1 1];
end

%% Determine best fit coefficients
fSSR = @(coefs, x, y) sum(( y - yeqn(coefs, x) ).^2);
[MyCoefs, Sr] = fminsearch(@(MyCoefsD) fSSR(MyCoefsD, x, y), InitGuess);

%% Generate estimates models
yhat   = yeqn(MyCoefs, x);
ymodel = yeqn(MyCoefs, xmodel);

%% Calculate statistics
ybar = mean(y);
St = sum((y-ybar).^2)
Sr = sum((y-yhat).^2)
r2 = (St - Sr) / St

%% Plots
plot(x,      y,      'ko', ...
     x,      yhat,   'k*',...
     xmodel, ymodel, 'k-')
