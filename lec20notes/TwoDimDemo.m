clear; format short e

load DOCtable
OC = rand(size(OC))
N = 50;
[cmodel, Tmodel] = meshgrid(...
    linspace(min(c(:)), max(c(:)), N), ...
    linspace(min(T(:)), max(T(:)), N));

figure(1); clf
surfc(c, T, OC);
view([145 15])
colormap copper
figure(2); clf
surfc(cmodel, Tmodel, interp2(c, T, OC, cmodel, Tmodel, 'bicubic'));
hold on
plot3(c, T, OC, 'b*')
hold off
view([145 15])
colormap copper