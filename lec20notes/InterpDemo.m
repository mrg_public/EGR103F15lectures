clear; format short e
time = [ 0  8 11 17 22]';
Temp = [ 1  3  6 11  8]';

%% Use these to show oscillation issue with jumps
% time = [ 1 2 3 4 5 6 7 8  9]';
% Temp = [ 1 2 3 4 5 6 7 8 10]';

x = time; 
% Later add more seconds to show shift
x = x + 24*30*12*100;

y = Temp;
xmodel = linspace(min(x), max(x), 500);

% Generate interpolations
ymodelNN  = interp1(x, y, xmodel, 'nearest');
ymodelLIN = interp1(x, y, xmodel, 'linear');
ymodelP   = polyval(polyfit(x, y, length(x)-1), xmodel);

plot(x,      y,         'ko', ...
     xmodel, ymodelNN,  'k.', ...
     xmodel, ymodelLIN, 'k-', ...
     xmodel, ymodelP  , 'k--')
gzoom
legend('Data', 'Nearest', 'Linear', 'Polynomial', 0)