%% Initialize workspace
clear; format short e;

%% Load and manipulate data; 
% Population data from Chapra
years = [1920:10:2000]';
pops  = [106.46 123.08 132.12 152.27 180.67 205.05 227.23 249.46 281.24]';
%rand('seed', 11052012)
%pops = rand(size(years));

%% Rename and create model data
calendar = 'Gregorian Shift'
switch calendar
    case 'Gregorian'
        years = years;
    case 'Anno Hegirae'
        years = (years + (196/365) - 622) * 365.2425 / (12*29.530588);
    case 'Anno Mundi'
        years = years - 1240 + 5000;
    case 'Gregorian Shift'
        years = years-years(1);
    otherwise
        error('Unknown scaling')
end
x = years;
y = pops;
xmodel = linspace(min(x), max(x));

%% Generate model
ymodelP = polyval(polyfit(x,y,length(x)-1), xmodel);
ymodelL = interp1(x, y, xmodel, 'linear');

%% Generate plots
figure(1); clf
plot(x,      y,         'k*',...
     xmodel, ymodelP,   'k-',...
     xmodel, ymodelL,   'k:')
xlabel('Independent Value');
ylabel('Dependent Value');
title('Dependent vs. Independent and Model')
legend('Data', 'Polynomial', 'Linear', 0)
gzoom