clear; format short e

load Cantilever.dat

Force = Cantilever(:,1) * 9.81;
Disp  = Cantilever(:,2) * 0.0254;

x = Force;
y = Disp;

%% Generate the independent model
NumPts = 100;
xmodel = linspace(min(x), max(x), NumPts);

%% Determine the model
model = 'exp'
switch model
    case 'linear'
        yeqn = @(coefs, x) coefs(1)*x.^1 + coefs(2)*x.^0;
        A    =                     [x.^1            x.^0];
    case 'line through origin'
        yeqn = @(coefs, x) coefs(1)*x.^1;
        A    =                      [x.^1];
    case 'trig'
        yeqn = @(coefs, x) coefs(1)*sin(x) + coefs(2)*cos(x);
        A    =                     [sin(x)            cos(x)]
    case 'trig 2'
        yeqn = @(coefs, x) coefs(1)*sin(0.1*x) + coefs(2)*cos(0.1*x);
        A    =                     [sin(0.1*x)            cos(0.1*x)]
    case 'exp'
        yeqn = @(coefs, x) coefs(1)*exp(x);
        A    =                     [exp(x)];
end

%% Determine coefficients
MyCoefs = A\y % inv(A'*A)*A'*y

%% Generate estimates and models
yhat   = yeqn(MyCoefs, x);
ymodel = yeqn(MyCoefs, xmodel);

%% Calculate stats
ybar = mean(y);
St = sum( ( y - ybar).^2 )
Sr = sum( ( y - yhat).^2 )
r2 = (St - Sr) / St

%% Plotting
plot(x,      y,      'ko', ...
     xmodel, ymodel, 'k-', ...
     x,      yhat,   'k*')