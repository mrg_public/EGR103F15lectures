clear; format short e

load Cantilever.dat

Force = Cantilever(:,1) * 9.81;
Disp  = Cantilever(:,2) * 0.0254;

x = Force;
y = Disp;
% rand('seed', 10192015)
% y = y + 0.01 * (rand(size(y)) - 0.5);
% y(4) = 3*y(4);

%% Generate independent model
NumPts = 100;
xmodel = linspace(min(x), 3*max(x), NumPts);

Order = 7;
P = polyfit(x, y, Order);

%% Generate estimates and models
yhat   = polyval(P, x);
ymodel = polyval(P, xmodel);

%% Calculate statistical measure of goodness of fit
ybar = mean(y);
St = sum( ( y - mean(y) ).^2 )
Sr = sum( ( y - yhat ).^2 )
r2 = (St - Sr) / St

%% Plotting
plot(x,      y,      'ko', ...
     xmodel, ymodel, 'k-', ...
     x,      yhat,   'k*')

