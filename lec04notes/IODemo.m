
%x = rand(3)
%x

%disp(x)

%fprintf('2 + 2 = 4\n \\ \n')
%fprintf('Don''t do that!\n')

%x = input(' First number: ');
%y = input('Second number: ');

%fprintf('The sum of %f and %f is %f\n', x, y, x+y)

MyNum = pi;
fprintf('The number is: %f\n', MyNum)
fprintf('The number is: %6.0f\n', MyNum)
fprintf('The number is: %6.2f\n', 1e6*MyNum)
fprintf('The number is: %+6.0f\n', MyNum)

fprintf('The number is: %e\n', MyNum)
fprintf('The number is: %16.0e\n', MyNum)
fprintf('The number is: %16.2e\n', 1e6*MyNum)
fprintf('The number is: %+16.0e\n', MyNum)
fprintf('The number is: %+16.0e\n', realmax)