%% Elementary matrices and manipulation
%  help elmat

ZeroSq   = zeros(3)
ZeroRect = zeros(4, 5)
OnesSq   = ones(3)
OnesRect = ones(4, 5)
EyeSq    = eye(3)
EyeRect  = eye(4, 5)
LinTwoArg = linspace(0, 3);
LinThreeArg = linspace(-5, 1, 20);
LogTwoArg = logspace(0, 5);
% colon operator

%% Basic array information
[SizeOR] = size(OnesRect)
[SizeR, SizeC] = size(OnesRect)
SizeRows = size(OnesRect, 1)
SizeCols = size(OnesRect, 2)
length(OnesRect)
numel(OnesRect)

A = rand(6)
A([1 2 3 4 5 6], 2)
A(1, end)
A([1, end], 4)
A( 2:2:end, 1:2:end )
A( :, 2)