MyFun = @(xa, ya, za) cos(xa) + sin(ya) + za
xVals = linspace(-pi, 2*pi, 200);
yVal  = pi;
for k=1:length(xVals)
     [zVals(k), fVals(k)] = fzero(@(zDummy) MyFun(xVals(k), yVal, zDummy), 12);
end