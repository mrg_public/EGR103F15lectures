% [Function or Script Name]
% [Your Name]
% [Date Modified]

% I have adhered to all the tenets of the 
% Duke Community Standard in creating this code.
% Signed: [Your NetID]

%% Initialize workspace

%% Load data
MyData = load('NRELECSU2013.dat');

%% Split data into vectors whose names make sense
Day  = % add code here to extract correct info
Hour = % add code here to extract correct info
Irr  = % add code here to extract correct info
Temp = % add code here to extract correct info
Wind = % add code here to extract correct info

%% Analyze data
%  Find which element has the max irradiance, where it is in 
%  the vector, and use that to print information about its value, 
%  and the day and hour it happened
%  Add code here to figure out what the max value is - call
%  that Val - and in which element - call that Loc - then use that
%  in the fprintf below
Val = 
Loc = 
fprintf('Max irradiance of %+0.2e W/m^2 on day %3.0f and hour %2.0f\n',...
    Val, Day(Loc), Hour(Loc))

%  Find the value and element location of the max avg. hourly temp,
%  then print out that information

%  Find the value and element location of the min avg. hourly temp,
%  then print out that information

%  Find the value and element location of the max avg. hourly wind speed,
%  then print out that information

%% Determine how many hours had a wind speed of zero and report that
NoWind = % Your code here;
fprintf('Average wind speed was 0 m/sec for %4.0f hours\n', NoWind);

%% Determine how many hours the temp was below 0C and report that


%% Plot data
%  Post-Noon-Hour Irradiance for 2013
figure(1), clf
% plot command goes here
xlabel('Day of Year');
ylabel('Irradiance, W/m^2')
title('Post-Noon-Hour Irradiance for 2013')
print -deps WeatherPlot1 

%  Hourly Average Temperatures for January 2013
figure(2), clf
%  Code goes here
print -deps WeatherPlot2

%  25 Fastest Average Hourly Wind Speeds
figure(3), clf
%  Code goes here
print -deps WeatherPlot3